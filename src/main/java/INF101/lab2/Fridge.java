package INF101.lab2;



import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


public class Fridge implements IFridge{
	
	ArrayList<FridgeItem> Items = new ArrayList<FridgeItem>(20);
	
	public Fridge() {
		Items = new ArrayList<FridgeItem>(20);
		
	}

	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return this.Items.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return 20;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (nItemsInFridge()== totalSize()) {
			return false;
		}
		else {
			Items.add(item);
			return true;
		}
	}

	@Override
	public void takeOut(FridgeItem item) {
		// TODO Auto-generated method stub
		if (Items.contains(item)){
			Items.remove(item);
		}
		
		else {
		throw new NoSuchElementException();
		}
		
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
		Items.removeAll(Items);
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> ListB = (ArrayList<FridgeItem>) this.Items.clone();
		
		
		for (FridgeItem item : Items) {
		
			 if(!item.hasExpired()) {
				 ListB.remove(item);
			 }
			 
				 
			 
				
				
			
			
		}
		Items.removeAll(ListB);
		return ListB;
	}

}
